package com.baomidou.crab.sys.mapper;

import com.baomidou.crab.sys.entity.Log;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统日志表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2018-10-06
 */
public interface LogMapper extends BaseMapper<Log> {

}
